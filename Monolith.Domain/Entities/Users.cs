﻿using System;
using System.Collections.Generic;

namespace Monolith.Domain.Entities
{
    public partial class Users
    {
        public int Userid { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
