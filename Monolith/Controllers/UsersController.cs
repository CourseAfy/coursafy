﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Monolith.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UsersController : ControllerBase
    {

        [HttpPost]
        public async Task<IActionResult> CreateNewUser()
        {
            if(!ModelState.IsValid)
            {
                return BadRequest("Unable to create User");
            }



            return Ok();
        }
    }
}